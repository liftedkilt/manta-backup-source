package main

import (
	"fmt"
	"os"

	"gitlab.com/liftedkilt/manta-backup-source/client"
)

func main() {
	if len(os.Args) < 2 {
		fmt.Println("call with host:port, key as args")
		os.Exit(0)
	}
	hosts, _ := client.GetMounts(os.Args[1], os.Args[2])

	fmt.Println(hosts)
}
