#!/bin/bash

PROJPATH=$GOPATH/src/gitlab.com/liftedkilt/manta-backup-source


function cleanBin() {
    rm -rf $PROJPATH/bin/*
}

function cleanProto() {
    rm -rf $PROJPATH/pb/*.go
}

function cleanAll() {
    cleanBin
    cleanProto
}

function buildProto() {
    protoc --go_out=plugins=grpc:pb $PROJPATH/pb/*.proto -I $PROJPATH/pb/
}

function buildServer() {
    go build -o $PROJPATH/bin/manta-backup-source $PROJPATH/server/main.go
}

function buildAll() {
    buildProto
    buildServer
}

function main() {
    if [ "$1" == "build" ]; then
        if [ "$2" == "server" ]; then
            buildServer
        elif [ $2 == "proto" ]; then
            buildProto
        elif [ $2 == "all" ]; then
            buildAll
        fi
    elif [ "$1" == "clean" ]; then
        if [ "$2" == "bin" ]; then
            cleanBin
        elif [ $2 == "proto" ]; then
            cleanProto
        elif [ $2 == "all" ]; then
            cleanAll
        fi
    elif [ "$1" == "all" ]; then
        cleanAll
        buildAll
    fi
}

main "$@"