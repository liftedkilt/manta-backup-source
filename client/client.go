package client

import (
	"errors"
	"log"
	"time"

	pb "gitlab.com/liftedkilt/manta-backup-source/pb"
	"golang.org/x/net/context"
	"google.golang.org/grpc"
)

// GetMounts returns a slice of strings with all mounts
func GetMounts(address, authkey string) ([]string, error) {
	// Set up a connection to the server.
	conn, err := grpc.Dial(address, grpc.WithInsecure())
	if err != nil {
		log.Fatalf("did not connect: %v", err)
	}
	defer conn.Close()
	p := pb.NewGetMountsClient(conn)

	// Contact the server and print out its response.

	ctx, cancel := context.WithTimeout(context.Background(), time.Second)
	defer cancel()
	r, err := p.GetMounts(ctx, &pb.SecretKey{Key: authkey})
	if err != nil {
		log.Fatalf("could not list mounts: %v", err)
	}

	var mounts []string
	for _, mount := range r.Mounts {
		mounts = append(mounts, mount.Mount)
	}

	if len(mounts) == 0 {
		err := errors.New("Mounts was empty")
		return nil, err
	}

	return mounts, nil
}
