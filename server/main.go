package main

import (
	"flag"
	"log"
	"net"
	"path/filepath"
	"strings"

	pb "gitlab.com/liftedkilt/manta-backup-source/pb"
	"golang.org/x/net/context"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

const (
	port = ":50051"
)

var authkey string
var mountsdir string

// server is used to implement helloworld.GreeterServer.
type server struct{}

func init() {
	flag.StringVar(&authkey, "key", "", "Specify a key to use for client verification")
	flag.StringVar(&mountsdir, "path", "/mnt/", "Path to mounts dir.")
	flag.Parse()
}

// GetMounts implements pb.GetMounts
func (s *server) GetMounts(ctx context.Context, key *pb.SecretKey) (*pb.Mounts, error) {
	response := &pb.Mounts{}
	if string(key.Key) == authkey {
		log.Println("key matches")

		mounts := []*pb.Mounts_Mount{}

		lines := getMountsDirs(mountsdir)
		for _, line := range lines {
			line = strings.TrimPrefix(line, mountsdir)
			Mount := &pb.Mounts_Mount{
				Mount: line,
			}
			mounts = append(mounts, Mount)
		}

		response = &pb.Mounts{
			Mounts: mounts,
		}
	} else {
		log.Println("key mismatch")
		response = &pb.Mounts{
			Mounts: []*pb.Mounts_Mount{},
		}
	}

	return response, nil
}

func main() {
	log.Println("Serving mounts from:", mountsdir)

	lis, err := net.Listen("tcp", port)
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}
	s := grpc.NewServer()
	pb.RegisterGetMountsServer(s, &server{})
	// Register reflection service on gRPC server.
	reflection.Register(s)
	if err := s.Serve(lis); err != nil {
		log.Fatalf("failed to serve: %v", err)
	}
}

func getMountsDirs(path string) []string {
	pathGlob := path + "*"
	mounts, err := filepath.Glob(pathGlob)
	if err != nil {
		log.Fatalln(err)
	}

	return mounts
}
